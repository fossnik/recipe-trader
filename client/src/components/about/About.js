import React from "react";
import selfie from "../../img/selfie-small.jpg";

const About = () => (
  <div className="row">
    <div className="col-md-12">
      <div className="card card-body bg-info text-black mb-3 text-center">
        <div className="text-center">
          <h1 className="display-3 mb-4">About</h1>
          <img src={selfie} alt="https://zackhartmann.dev" style={{
            "borderRadius": "50px",
            "maxWidth": "100%",
            "height": "auto",
            "display": "block",
            "margin": "auto"
          }}/>
          <article className="lead text-center m-4 card-header bg-white">
            My name is <a href="https://zackhartmann.dev">Zack Hartmann</a>
            <br/>
            I am a software engineer from Providence, RI.
            I designed this website as a demo for React.js
          </article>
        </div>
      </div>
    </div>
  </div>
);

export default About;
