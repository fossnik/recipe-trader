import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import classnames from "classnames";
import { deletePost, addLike, removeLike } from "../../actions/postActions";

class PostItem extends Component {
  onDeleteClick(id) {
    this.props.deletePost(id);
  }

  onLikeClick(id) {
    this.props.addLike(id);
  }

  onUnlikeClick(id) {
    this.props.removeLike(id);
  }

  findUserLike = likes =>
    likes.filter(like => like.user === this.props.auth.user.id).length > 0;

  render() {
    const { post, auth, showActions } = this.props;
    const handle = post.userDetails ? post.userDetails[0].handle : "";

    const actionButtons = <span>
        <button
          onClick={this.onLikeClick.bind(this, post._id)}
          type="button"
          className="btn btn-light mr-1">
          <i
            className={classnames("fas fa-thumbs-up", {
              "text-info": this.findUserLike(post.likes)
            })}
          />
          <span className="badge badge-light">{post.likes.length}</span>
        </button>
        <button
          onClick={this.onUnlikeClick.bind(this, post._id)}
          type="button"
          className="btn btn-light mr-1">
          <i className="text-secondary fas fa-thumbs-down"/>
        </button>
        <Link to={"/post/" + post._id} className="btn btn-info mr-1">
          Comments
        </Link>
      {post.user === auth.user.id ? (
        <button
          onClick={this.onDeleteClick.bind(this, post._id)}
          type="button"
          className="btn btn-danger mr-1">
          <i className="fas fa-times"/>
        </button>
      ) : null}
      </span>;

    return (
      <div className="card" style={{
        "minWidth": "285px",
        "margin": ".5em"
      }}>
        <div className="card-body">
          <Link to={"/post/" + post._id}>
            <h2 className="card-title text-center card-header">
              {post.recipeName}
            </h2>
          </Link>

          {post.image
            ? <img src={post.image} alt={post.recipeName} className="card-img-top"/>
            : (
              <div style={{
                "position": "relative",
                "background": "#ccc",
                "margin": "6px",
                "height": "4em",
                "borderRadius": "6px"
              }}>
                <i className="fas fa-pizza-slice fa-3x" style={{
                  "marginLeft": "47%",
                  "paddingTop": "14px"
                }}/>
              </div>
            )
          }

          <h4 className="card-header m-2">Ingredients</h4>
          <ul className="card-text m-3">
            {post.ingredients.map((ingredient, index) => <li key={`${index}-${ingredient}`}>{ingredient}</li>)}
          </ul>

          <h4 className="card-header m-2">Directions</h4>
          <ul className="card-text m-3">
            {post.directions.map((direction, index) => <li key={`${index}-${direction}`}>{direction}</li>)}
          </ul>

          {showActions ? actionButtons : null}

          <div className="card-footer float-right">
            By <Link to={"/profile/" + handle}>{handle}</Link>
          </div>
        </div>
      </div>
    );
  }
}

PostItem.defaultProps = {
  showActions: true
};

PostItem.propTypes = {
  deletePost: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { deletePost, addLike, removeLike }
)(PostItem);
