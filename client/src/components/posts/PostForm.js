import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import TextFieldGroup from "../common/TextFieldGroup";
import { addPost } from "../../actions/postActions";
import { SubmitButton } from "../common/Buttons";

class PostForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recipeName: "",
      ingredients: "",
      directions: "",
      image: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps({ errors }) {
    if (errors) this.setState({ errors });
  }

  onSubmit(e) {
    e.preventDefault();

    const { user } = this.props.auth;

    const newPost = {
      recipeName: this.state.recipeName,
      ingredients: this.state.ingredients,
      directions: this.state.directions,
      image: this.state.image,
      name: user.name,
    };

    this.props.addPost(newPost);

    this.setState({
      recipeName: "",
      ingredients: "",
      directions: "",
      image: "",
    });

    window.location.reload(true);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="post-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">Post a New Recipe</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <TextFieldGroup
                  placeholder="Recipe Name"
                  name="recipeName"
                  value={this.state.recipeName}
                  onChange={this.onChange}
                  error={errors.recipeName}
                />
                <TextAreaFieldGroup
                  placeholder="Required Ingredients"
                  name="ingredients"
                  value={this.state.ingredients}
                  onChange={this.onChange}
                  error={errors.ingredients}
                />
                <TextAreaFieldGroup
                  placeholder="Instructions"
                  name="directions"
                  value={this.state.directions}
                  onChange={this.onChange}
                  error={errors.directions}
                />
                <TextFieldGroup
                  placeholder="Link to JPEG Image"
                  name="image"
                  value={this.state.image}
                  onChange={this.onChange}
                  error={errors.image}
                />
              </div>
              <SubmitButton/>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

PostForm.propTypes = {
  addPost: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addPost }
)(PostForm);
