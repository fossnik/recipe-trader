import React, { Component } from "react";
import { isEmpty } from "../../validation/is-empty";

class ProfileHeader extends Component {
  render() {
    const { profile } = this.props;

    const socialMediaIcons = <p>
      {isEmpty(profile.social && profile.social.website) ? null : (
        <a
          className="text-white p-2"
          href={profile.social.website}
          target="_blank"
          rel="noopener noreferrer"
        >
          <i className="fas fa-globe fa-2x" />
        </a>
      )}
      {isEmpty(profile.social && profile.social.twitter) ? null : (
        <a
          className="text-white p-2"
          href={profile.social.twitter}
          target="_blank"
          rel="noopener noreferrer"
        >
          <i className="fab fa-twitter fa-2x" />
        </a>
      )}
      {isEmpty(profile.social && profile.social.facebook) ? null : (
        <a
          className="text-white p-2"
          href={profile.social.facebook}
          target="_blank"
          rel="noopener noreferrer"
        >
          <i className="fab fa-facebook fa-2x" />
        </a>
      )}
      {isEmpty(profile.social && profile.social.linkedin) ? null : (
        <a
          className="text-white p-2"
          href={profile.social.linkedin}
          target="_blank"
          rel="noopener noreferrer"
        >
          <i className="fab fa-linkedin fa-2x" />
        </a>
      )}
    </p>;

    return (
      <div className="row">
        <div className="col-md-12">
          <div className="card card-body bg-info text-white mb-3">
            <div className="text-center">
              <h1 className="display-4 text-center">{profile.user.name}</h1>
              <p className="lead text-center">
                {" "}
                {isEmpty(profile.status) ? null : <span>{profile.status}</span>}
              </p>
              {isEmpty(profile.location) ? null : <p>{profile.location}</p>}
              {socialMediaIcons}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileHeader;
