import React, { Component } from "react";
import PropTypes from "prop-types";
import { isEmpty } from "../../validation/is-empty";

class ProfileAbout extends Component {
  render() {
    const { profile } = this.props;

    const firstName = profile.user.name.trim().split(" ")[0];

    const skillsBlock = typeof profile.skills === "undefined" ? null : <>
      <hr/>
      <h3 className="text-center text-info">Expertise</h3>
      <div className="row">
        <div className="d-flex flex-wrap justify-content-center align-items-center">
          {profile.skills.map((skill, index) => (
            <div key={index} className="p-3">
              <i className="fa fa-check"/>
              {skill}
            </div>
          ))}
        </div>
      </div>
    </>;

    const socialMediaBlock = typeof profile.social === "undefined" ? null : <>
      <hr/>
      <h3 className="text-center text-info">Social Media</h3>
      {isEmpty(profile.social.website) ? null : <p>
        <a href={profile.social.website}>{profile.social.website}</a>
      </p>}
      {isEmpty(profile.social.twitter) ? null : <p>
        <a href={profile.social.twitter}>{profile.social.twitter}</a>
      </p>}
      {isEmpty(profile.social.facebook) ? null : <p>
        <a href={profile.social.facebook}>{profile.social.facebook}</a>
      </p>}
      {isEmpty(profile.social.linkedin) ? null : <p>
        <a href={profile.social.linkedin}>{profile.social.linkedin}</a>
      </p>}
    </>;

    return (
      <div className="row">
        <div className="col-md-12">
          <div className="card card-body bg-light mb-3">
            <h3 className="text-center text-info">About {firstName}</h3>
            <p className="lead">
              {isEmpty(profile.bio) ? (
                <span>{firstName} does not have a bio</span>
              ) : (
                <span>{profile.bio}</span>
              )}
            </p>
            {isEmpty(profile.skills) ? null : skillsBlock}
            {isEmpty(profile.social) ? null : socialMediaBlock}
          </div>
        </div>
      </div>
    );
  }
}

ProfileAbout.propTypes = {
  profile: PropTypes.object.isRequired
};

export default ProfileAbout;
