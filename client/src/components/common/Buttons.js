import React from "react";

export const SubmitButton = () => (
  <div className="row align-items-center justify-content-center mb-1">
    <input type="submit" className="btn btn-info btn-block w-75" value="Submit"/>
  </div>
);


