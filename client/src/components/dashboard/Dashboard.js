import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCurrentProfile, deleteAccount } from "../../actions/profileActions";
import Spinner from "../common/Spinner";
import ProfileActions from "./ProfileActions";

class Dashboard extends Component {
  componentDidMount() {
    this.props.getCurrentProfile();
  }

  onDeleteClick(e) {
    this.props.deleteAccount();
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;

    let dashboardContent;

    if (profile === null || loading) {
      dashboardContent = <Spinner/>;
    } else {
      // check if logged-in user has data
      if (Object.keys(profile).length > 0) {
        dashboardContent = (
          <div className="col-md-12">
            <h2 className="p-4">
              Welcome <Link to={`/profile/${profile.handle}`}>{user.name}</Link>
            </h2>
            <div>
              <Link to="/feed">
                <button>Browse recipes</button>
              </Link>
              <Link to="/profiles">
                <button>Browse contributors</button>
              </Link>
            </div>
            <div style={{ margin: "20px" }}>
              <p>This is your dashboard. From here, you can manage your public profile, or delete your account</p>
            </div>
            <ProfileActions/>
            <br/>
            <button
              onClick={this.onDeleteClick.bind(this)}
              className="btn btn-danger"
            >
              Delete My Account
            </button>
          </div>
        );
      } else {
        // user is logged in, but has no profile
        dashboardContent = (
          <div className="col-md-12">
            <h2 className="p-4">Welcome {user.name}</h2>
            <p>We hope you enjoy Recipe Trader. Click below to create your profile.</p>
            <Link to="/set-profile" className="btn btn-lg btn-info">
              Create Profile
            </Link>
          </div>
        );
      }
    }

    return (
      <div className="dashboard text-center">
        <div className="container jumbotron">
          <div className="row">
            {dashboardContent}
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, deleteAccount }
)(Dashboard);
