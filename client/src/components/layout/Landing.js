import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";

import OrangeLogo from "../../img/RecipeTrader-orange.png";

class Landing extends Component {
  componentDidMount() {
    if (this.props.auth.isAuthenticated) this.props.history.push("/dashboard");
  }

  render = () => {
    const LandingPad = styled.div`
      position: relative;
      background-size: cover;
      background-position: center;
      height: 100vh;
    `;

    const DarkOverlay = styled.div`
      background-color: rgba(0, 0, 0, 0.7);
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    `;

    const LogoLander = styled.img`
      width: 70%;
      max-width: 600px;
    `;

    return <LandingPad>
      <DarkOverlay className="text-light">
        <div className="row">
          <div className="col-md-12 text-center">
            <LogoLander src={OrangeLogo}/>
            <div className="m-2">
              <Link to="/register" className="btn btn-info mr-2">
                Sign Up
              </Link>
              <Link to="/login" className="btn btn-light">
                Login
              </Link>
            </div>
          </div>
        </div>
      </DarkOverlay>
    </LandingPad>;
  };
}

Landing.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({ auth: state.auth });

export default connect(mapStateToProps)(Landing);
