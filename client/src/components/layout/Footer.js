import React from "react";

const footerStyle = {
  position: "fixed",
  bottom: "0",
  left: "0",
  width: "100%",
};

export default () => (
  <footer className="p-2 text-center bg-light" style={footerStyle}>
    &copy; Zack Hartmann {new Date().getFullYear()}
  </footer>
);
