import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { clearCurrentProfile } from "../../actions/profileActions";
import styled from "styled-components";

import RecipeTraderLogoSVG from "../../img/RecipeTrader-logo.png";

const LogoHeader = styled.img`
	borderRadius: 10px;
`;

const mapStateToProps = state => ({ auth: state.auth });

class Navbar extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render = () => (
    <nav className="navbar navbar-expand-lg navbar-dark">
      <Link className="navbar-brand" to="/">
        <LogoHeader
          alt="RecipeTrader Logo"
          src={RecipeTraderLogoSVG}
          className="icon"/>
      </Link>

      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"/>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav">
          <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <Link className="nav-link btn" to="/profiles">
              <i className="fas fa-user-circle"/>&nbsp;Contributors
            </Link>
          </li>
          <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <Link className="nav-link btn" to="/feed">
              <i className="fas fa-pizza-slice"/>&nbsp;Recipes
            </Link>
          </li>
          <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
            <Link className="nav-link btn" to="/about">
              <i className="fas fa-info-circle"/>&nbsp;About
            </Link>
          </li>
        </ul>

        {this.props.auth.isAuthenticated ? (
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <div className="nav-link btn dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-cog"/>&nbsp;{this.props.auth.user.name}
              </div>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link className="dropdown-item nav-link" to="/dashboard">
                  <span>My Account</span>
                </Link>
                <div className="dropdown-divider"/>
                <a className="dropdown-item nav-link"
                   href="//login"
                   onClick={this.onLogoutClick.bind(this)}>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        ) : (
          <ul className="navbar-nav">
            {
              window.location.pathname === "/register" ? null : (
                <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
                  <Link className="nav-link btn" to="/register">
                    Sign Up
                  </Link>
                </li>
              )
            }
            {
              window.location.pathname === "/login" ? null : (
                <li className="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
                  <Link className="nav-link btn" to="/login">
                    Login
                  </Link>
                </li>
              )
            }
          </ul>
        )}
      </div>
    </nav>
  );
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps,
  { logoutUser, clearCurrentProfile }
)(Navbar);
