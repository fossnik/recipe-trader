import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import InputGroup from "../common/InputGroup";
import SelectListGroup from "../common/SelectListGroup";
import { createProfile, getCurrentProfile } from "../../actions/profileActions";
import { isEmpty } from "../../validation/is-empty";
import { SubmitButton } from "../common/Buttons";

class SetProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      displaySocialInputs: false,
      handle: "",
      location: "",
      status: "",
      skills: "",
      bio: "",
      website: "",
      twitter: "",
      facebook: "",
      linkedin: "",
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) this.setState({ errors: nextProps.errors });

    if (nextProps.profile.profile) {
      const { profile } = nextProps.profile;

      // set component fields state
      this.setState({
        handle: profile.handle || "",
        location: profile.location || "",
        status: profile.status || "",
        skills: isEmpty(profile.skills) ? "" : profile.skills.join(","),
        bio: profile.bio || "",
        website: profile.social ? profile.social.website : "",
        twitter: profile.social ? profile.social.twitter : "",
        facebook: profile.social ? profile.social.facebook : "",
        linkedin: profile.social ? profile.social.linkedin : ""
      });
    }
  }

  onSubmit = e => {
    e.preventDefault();

    const profileData = {
      handle: this.state.handle,
      location: this.state.location,
      status: this.state.status,
      bio: this.state.bio,
      skills: this.state.skills,
      social: {}
    };

    if (!isEmpty(this.state.skills)) profileData.skills = this.state.skills;
    if (!isEmpty(this.state.website)) profileData.social.website = this.state.website;
    if (!isEmpty(this.state.twitter)) profileData.social.twitter = this.state.twitter;
    if (!isEmpty(this.state.linkedin)) profileData.social.linkedin = this.state.linkedin;
    if (!isEmpty(this.state.facebook)) profileData.social.facebook = this.state.facebook;

    this.props.createProfile(profileData, this.props.history);
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { errors, displaySocialInputs } = this.state;

    // Select options for status
    const options = [
      { label: "* Select Professional Status", value: 0 },
      { label: "Professional Chef", value: "Professional Chef" },
      { label: "Food Enthusiast", value: "Food Enthusiast" },
      { label: "Culinary Hobbyist", value: "Culinary Hobbyist" },
      { label: "Other", value: "Other" }
    ];

    return <div className="set-profile">
      <div className="container jumbotron">
        <div className="row">
          <div className="col-md-8 auto ml-auto mr-auto">
            <Link to="/dashboard" className="btn btn-light">
              Go Back
            </Link>
            <h1 className="display-4 text-center">Your Profile</h1>
            <p className="lead text-center">
              Provide information for your public profile
            </p>
            <small className="d-block pb-3">* = required</small>
            <form onSubmit={this.onSubmit}>
              <TextFieldGroup
                placeholder="* Profile Handle"
                name="handle"
                value={this.state.handle}
                onChange={this.onChange}
                error={errors.handle}
                info="This is the username that will appear to others for your account"
              />
              <SelectListGroup
                placeholder="Status"
                name="status"
                value={this.state.status}
                onChange={this.onChange}
                options={options}
                error={errors.status}
              />
              <TextFieldGroup
                placeholder="Location"
                name="location"
                value={this.state.location}
                onChange={this.onChange}
                error={errors.location}
              />
              <TextFieldGroup
                placeholder="Skills"
                name="skills"
                value={this.state.skills}
                onChange={this.onChange}
                error={errors.skills}
              />
              <TextAreaFieldGroup
                placeholder="Biographic"
                name="bio"
                value={this.state.bio}
                onChange={this.onChange}
                error={errors.bio}
              />
              <div className="mb-3">
                <button
                  type="button"
                  className="btn btn-light"
                  onClick={() =>
                    this.setState(prevState => ({
                      displaySocialInputs: !prevState.displaySocialInputs
                    }))}
                >
                  Add Social Media
                </button>
              </div>
              {
                displaySocialInputs ?
                  <div>
                    <InputGroup
                      placeholder="Personal Website"
                      name="website"
                      icon="fas fa-globe"
                      value={this.state.website || ""}
                      onChange={this.onChange}
                      error={errors.website}
                    />
                    <InputGroup
                      placeholder="Twitter Profile URL"
                      name="twitter"
                      icon="fab fa-twitter"
                      value={this.state.twitter || ""}
                      onChange={this.onChange}
                      error={errors.twitter}
                    />
                    <InputGroup
                      placeholder="Facebook Profile URL"
                      name="facebook"
                      icon="fab fa-facebook"
                      value={this.state.facebook || ""}
                      onChange={this.onChange}
                      error={errors.facebook}
                    />
                    <InputGroup
                      placeholder="Linkedin Profile URL"
                      name="linkedin"
                      icon="fab fa-linkedin"
                      value={this.state.linkedin || ""}
                      onChange={this.onChange}
                      error={errors.linkedin}
                    />
                  </div> : null
              }
              <SubmitButton/>
            </form>
          </div>
        </div>
      </div>
    </div>;
  }
}

SetProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { createProfile, getCurrentProfile }
)(withRouter(SetProfile));
