import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import { clearCurrentProfile } from "./actions/profileActions";
import { Provider } from "react-redux";
import store from "./store";

import PrivateRoute from "./components/common/PrivateRoute";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Dashboard from "./components/dashboard/Dashboard";
import SetProfile from "./components/set-profile/SetProfile";
import Profiles from "./components/profiles/Profiles";
import Profile from "./components/profile/Profile";
import Posts from "./components/posts/Posts";
import Post from "./components/post/Post";
import About from "./components/about/About";
import NotFound from "./components/not-found/NotFound";
import "./App.css";

import Background from "../src/img/bg.jpg";

// check for authentication jwt token
if (localStorage.jwtToken) {
  // set auth token header 'auth'
  setAuthToken(localStorage.jwtToken);

  // decode token and get user info and expiration
  const decoded = jwt_decode(localStorage.jwtToken);

  // set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  if (decoded.exp * 1000 < Date.now()) {
    // logout user if expired
    store.dispatch(logoutUser());

    // clear current profile
    store.dispatch(clearCurrentProfile());

    // redirect to login
    window.location.href = "/login";
  }
}

export default class App extends Component {
  componentDidMount() {
    document.body.style.backgroundImage = `url(${Background})`
  }

  render = () => (
    <Provider store={store}>
      <Router>
        <Navbar/>
        <Route exact path="/" component={Landing}/>
        <div>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/profiles" component={Profiles}/>
          <Route exact path="/profile/:handle" component={Profile}/>
          <Route exact path="/feed" component={Posts}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/post/:id" component={Post}/>
          <PrivateRoute exact path="/dashboard" component={Dashboard}/>
          <PrivateRoute exact path="/set-profile" component={SetProfile}/>
          <Route exact path="/not-found" component={NotFound}/>
        </div>
        <Footer/>
      </Router>
    </Provider>
  );
}
