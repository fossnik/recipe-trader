import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { GET_ERRORS, SET_CURRENT_USER } from "./types";

// register user
export const registerUser = (userData, history) => dispatch => {
  axios
    .post("/api/users/register", userData)
    .then(() => history.push("/login"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {...err.response.data}
      })
    );
};

// Login - Get user token
export const loginUser = userData => dispatch => {
  axios
    .post("/api/users/login", userData)
    .then(res => {
      // Save to localStorage
      const { token } = res.data;

      // set token to localstorage
      // Localstorage only stores strings
      localStorage.setItem("jwtToken", token);

      // set token to Auth header
      setAuthToken(token);

      // decode token for user data
      const decoded = jwt_decode(token);

      // set current user
      dispatch(setCurrentUser(decoded));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {...err.response.data}
      })
    );
};

// set logged-in user
export const setCurrentUser = decoded =>
  Object.assign({
    type: SET_CURRENT_USER,
    payload: decoded
  });

export const logoutUser = () => dispatch => {
  // remove token from local storage
  localStorage.removeItem("jwtToken");

  // remove auth header for future requests
  setAuthToken(false);

  // set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser());
};
