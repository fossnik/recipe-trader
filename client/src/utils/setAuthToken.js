import axios from "axios";

const setAuthToken = token => {
  // set auth token for every request
  if (token) axios.defaults.headers.common["Authorization"] = token;
  // delete auth header
  else delete axios.defaults.headers.common["Authorization"];
};

export default setAuthToken;
