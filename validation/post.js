const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validatePostInput(data) {
  let errors = {};

  // empty string will fail the validator
  data.recipeName = !isEmpty(data.recipeName) ? data.recipeName : "";

  if (!Validator.isLength(data.recipeName, { min: 2, max: 80 }))
    errors.recipeName = "Recipe name must be between 2 and 80 characters";

  if (Validator.isEmpty(data.recipeName))
    errors.recipeName = "Text field is required";

  data.ingredients = !isEmpty(data.ingredients) ? data.ingredients : "";

  if (!Validator.isLength(data.ingredients, { min: 10, max: 4000 }))
    errors.ingredients = "Ingredients must be between 10 and 4000 characters";

  if (Validator.isEmpty(data.ingredients))
    errors.ingredients = "Text field is required";

  data.directions = !isEmpty(data.directions) ? data.directions : "";

  if (!Validator.isLength(data.directions, { min: 10, max: 8000 }))
    errors.directions = "Directions must be between 10 and 8000 characters";

  if (Validator.isEmpty(data.directions))
    errors.directions = "Text field is required";

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
