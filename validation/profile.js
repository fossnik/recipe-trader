const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProfileInput(data) {
  let errors = {};

  // empty string will fail the validator
  data.handle = !isEmpty(data.handle) ? data.handle : "";
  data.status = !isEmpty(data.status) ? data.status : "";

  if (!Validator.isLength(data.handle, { min: 2, max: 40 }))
    errors.handle = "Handle needs to be between two and four characters";

  if (Validator.isEmpty(data.handle))
    errors.handle = "Profile handle required";

  if (Validator.isEmpty(data.status))
    errors.status = "Status required";

  if (data.hasOwnProperty("social")) {
    const { social } = data;
    if (!isEmpty(social.website) && !Validator.isURL(social.website))
      errors.website = "Invalid URL";
    if (!isEmpty(social.twitter) && !Validator.isURL(social.twitter))
      errors.twitter = "Invalid URL";
    if (!isEmpty(social.facebook) && !Validator.isURL(social.facebook))
      errors.facebook = "Invalid URL";
    if (!isEmpty(social.linkedin) && !Validator.isURL(social.linkedin))
      errors.linkedin = "Invalid URL";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
