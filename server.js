const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");

const users = require("./routes/api/users");
const profile = require("./routes/api/profile");
const posts = require("./routes/api/posts");

// create express instance
const app = express();

// Body parser middleware (for JSON parsing)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Mongo URI
const db = require("./config/keys").mongoURI;

// Connect to Mongo
mongoose
  .connect(db, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.error(err));

// Passport middleware
app.use(passport.initialize());

// Passport config - JWT
require("./config/passport")(passport);

// define routes
app.use("/api/users", users);
app.use("/api/profile", profile);
app.use("/api/posts", posts);

// PRODUCTION - Serve static assets
if (process.env.NODE_ENV === "production") {
  // set static folder
  app.use(express.static("client/build"));

  // all routes - load react index.html file
  app.get("*", (req, res) =>
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"))
  );
}

// Heroku deploy utilizes process.env.PORT
const port = process.env.PORT || 5000;

app.listen(port, () => console.log("Server running on port " + port));
