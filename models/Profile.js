const mongoose = require("mongoose");

module.exports = Profile = mongoose.model(
  "profile",
  new mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users" // reference the 'users' collection
    },
    handle: {
      type: String,
      required: true,
      max: 40
    },
    location: {
      type: String
    },
    status: {
      type: String,
      required: true
    },
    skills: {
      type: [String]
    },
    bio: {
      type: String
    },
    social: {
      website: {
        type: String
      },
      twitter: {
        type: String
      },
      facebook: {
        type: String
      },
      linkedin: {
        type: String
      }
    },
    date: {
      type: Date,
      default: Date.now
    }
  })
);
