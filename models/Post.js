const mongoose = require("mongoose");

const Post = mongoose.model(
  "posts",
  new mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users"
    },
    recipeName: {
      type: String,
      required: true
    },
    ingredients: {
      type: [String],
      required: true
    },
    directions: {
      type: [String],
      required: true
    },
    image: {
      type: String,
      required: false
    },
    likes: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "users"
        }
      }
    ],
    comments: [
      {
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "users"
        },
        text: {
          type: String,
          required: true
        },
        name: {
          type: String
        },
        date: {
          type: Date,
          default: Date.now
        }
      }
    ],
    date: {
      type: Date,
      default: Date.now
    }
  })
);

module.exports = Post;
