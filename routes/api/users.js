// login and authentication
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// Load keys
const keys = require("../../config/keys");

// Load Mongoose Model
const User = require("../../models/User");

// @route   GET api/users/test
// @desc    Tests users route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "Users works" }));

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  if (!isValid) return res.status(400).json(errors);

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = "Email already exists";
      return res.status(400).json(errors);
    }

    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    });

    // generate the encrypted password
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser
          .save()
          .then(user => res.json(user)) // respond with the user
          .catch(err => console.error(err));
      });
    });
  });
});

// @route   POST api/users/login
// @desc    Log-in user / Returning JWT
// @access  Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  if (!isValid) return res.status(400).json(errors);

  const email = req.body.email;
  const password = req.body.password;

  // find user (by email)
  User.findOne({ email }).then(user => {
    if (!user) return res.status(404).json({ email: "User not found" });

    // verify given password against stored password
    bcrypt.compare(password, user.password).then(isMatch =>
      !isMatch
        ? res.status(400).json({ password: "Password incorrect" })
        : jwt.sign(
            { // JWT Payload
              id: user.id,
              name: user.name,
            },
            keys.secretOrKey,
            { expiresIn: 60 * 60 * 12 }, // twelve hours
            (err, token) =>
              res.json({ success: true, token: "Bearer " + token })
          )
    );
  });
});

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  ({ user }, res) =>
    res.json({ id: user.id, name: user.name, email: user.email })
);

module.exports = router;
